# @summary Wrapper class for the epfl-accounts module
#
# Filter existing users to avoid creating group with 
# inexistant users.
#
class accounts {
  Accounts::User <| |> -> Accounts::Group <| |>
}
