# @summary Create a user
#
# Create his home folder and add SSH Pulic key from Hiera to
# authorized_keys file
#
# @example
#   include accounts
#   accounts::user { ['user1', 'user2'] }
#
define accounts::user {
  $sshkeys = lookup('accounts::sshkeys')[$name]
  $email  = $sshkeys['email']
  $type   = $sshkeys['type']
  $public = $sshkeys['public']

  user { $name:
    ensure     => present,
    managehome => true,
    comment    => $email,
    # purge_ssh_keys => true,  # Bug https://tickets.puppetlabs.com/browse/MODULES-7592
  }

  file { "/home/${name}/.ssh":
    ensure => directory,
    owner  => $name,
    group  => $name,
    mode   => '0700'
  }
  -> ssh_authorized_key { $name:
    ensure => present,
    user   => $name,
    type   => $type,
    key    => $public,
    name   => "${name} (${email})"
  }
}
  