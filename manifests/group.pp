# @summary Create a group and add members to it
#
# @example
#   accounts::group { 'group': members => ['user1', 'user2']}
define accounts::group (
  $members
) {

  group { $name:
    ensure          => present,
    auth_membership => true,  # Will purge members not managed
    members         => $members,
    provider        => 'gpasswd'  # Thanks to https://forge.puppet.com/onyxpoint/gpasswd
  }
}
