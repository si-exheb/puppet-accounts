# Changelog

All notable changes to this project will be documented in this file.


## Release 0.1.3 (2020-03-23)

Suppress the warning "data/common.yaml: file does not contain a valid yaml hash"


## Release 0.1.2

First working version