# epfl-accounts


#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with accounts](#setup)
    * [What accounts affects](#what-accounts-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with accounts](#beginning-with-accounts)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)


## Description

This Puppet module manage Linux user accounts creation. It add users SSH keys to their authorized_keys file. It also manage group.

It's simpler to use than puppetlabs-accounts and camptocamp-accounts. But also more limited.


## Setup


### Beginning with accounts

In Hiera, most probably in *data/common.yaml*, add your users keys:

```yaml
epfl_accounts::ssh_keys:
  user1:
    email: user1@epfl.ch
    type: ssh-rsa
    public: AAA...
```
The email will serve as a comment for the user. It add legibility in case the user name is a short version.


## Usage

To create a group, you need to pass a group name and a list of users. This will only work for existing users. If a user is missing, it will silently be ignored.

```puppet
include ::accounts
accounts::group {'<group>':   members => $accounts_list}
```

To create a user, provide a list of users:

```puppet
accounts::user { $accounts_list: }
```


## Limitations

Purge SSH key in user class doesn't work.

## Development

Please prefer to use PDK to contribute to this module.
